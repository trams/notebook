#!/usr/bin/env python
import requests
import csv
from os import listdir
from os.path import isfile, join, splitext
from datetime import datetime
import time
import psycopg2
from flask import Flask
app = Flask(__name__)

#CONSTANTS
API_KEY = '13877bf6-79a1-498a-8ec8-76f7be62493a'
TRAM_ID = 'c7238cfe-8b1f-4c38-bb4a-de386db7e776'
BUS_ID = 'f2e5503e-927d-4ad3-9500-4ab9e55deb59'
TRAM_URL = 'https://api.um.warszawa.pl/api/action/wsstore_get/'
BUS_URL = 'https://api.um.warszawa.pl/api/action/busestrams_get/'
DELAY = 30.0
TRAM_DIR = "tram"
BUS_DIR = "bus"
BUS_TYPE = 1
TRAM_ID_NAME = "id"
BUS_ID_NAME = "resource_id"

#CONSTANTS
DATA_DIR = "tram_data/"
NORMALIZED_DIR = "normalized/"
DB_NAME = "history"
PORT = 5432
USER = "ogj29ch@tramwaje"
PASSWORD = "{password_hidden}"
HOST = "tramwaje.postgres.database.azure.com"
RUNNING_STRING = "RUNNING"
TRAM_HEADER = ["Running", "FirstLine", "Lon", "Lines", "Time", "Lat", "LowFloor", "Brigade"]
BUS_HEADER = ["Lat", "Lon", "Time", "Lines", "Brigade"]


@app.route('/')
def hello_world():
    conn = psycopg2.connect(dbname=DB_NAME, port=PORT, user=USER,password=PASSWORD, host=HOST)
    cur = conn.cursor()
    cur.execute("SELECT time FROM trams ORDER BY time desc limit 1;")
    last_time_tram = cur.fetchall()[0]
    cur.execute("SELECT time FROM buses ORDER BY time desc limit 1;")
    last_time_buses = cur.fetchall()[0]
    conn.commit()
    cur.close()
    conn.close()
    return "Expected delay: %s | Last tram import: %s | Last buses import: %s" % (DELAY, last_time_tram[0], last_time_buses[0]) 

@app.route('/run')
def run():
    starttime=time.time()
    while True:
        gettram(TRAM_URL, TRAM_ID_NAME, TRAM_ID, API_KEY, TRAM_DIR)
        getbus(BUS_URL, BUS_ID_NAME, BUS_ID, API_KEY, BUS_DIR, BUS_TYPE)
        time.sleep(DELAY - ((time.time() - starttime) % DELAY))

def gettram(resource_url, id_name, resource_id, api_key, data_directory, type = None):
    # get json data
    start_time = time.time()
    start_time_date = datetime.now()
    r = requests.get(resource_url, params={id_name : resource_id, 'apikey' : api_key, 'type' : type})
    if r.status_code == requests.codes.ok:
        items = r.json()
        conn = psycopg2.connect(dbname=DB_NAME, port=PORT, user=USER,password=PASSWORD, host=HOST)
        cur = conn.cursor()
        file_name = datetime.now().strftime('%Y_%m_%d_%H_%M_%S.csv')
        with open(data_directory + file_name, 'a') as file:
            writer = csv.writer(file, delimiter = ';')
            writer.writerow(TRAM_HEADER)
            for i in items['result']: 
                try:          
                    row = [value for key, value in i.items()]
                    row_normalized = []
                    for x in row:
                        if isinstance(x,str):
                            row_normalized.append(x.strip())
                        else:
                            row_normalized.append(x)
                    pointString = "POINT(%s %s)" % (row_normalized[2], row_normalized[5])
                    cur.execute("INSERT INTO trams(running, firstLine, lines, time, lowFloor, brigade, geom,chunktime) VALUES (%s, %s, %s, %s, %s, %s, ST_GeomFromText(%s, 4326),%s)", [row_normalized[0] == RUNNING_STRING, row_normalized[1], row_normalized[3], row_normalized[4], row_normalized[6], row_normalized[7], pointString,start_time_date])
                    writer.writerow(row_normalized)
                except AttributeError as inst:
                    print(inst.args)      # arguments stored in .args
                    print(inst)    
        elapsed_time = time.time() - start_time
        cur.execute("INSERT INTO log(collection, time,chunktime) VALUES (%s, %s,%s)", ["Trams", elapsed_time, start_time_date])   
        conn.commit()
        cur.close()
        conn.close()
        print("TRAMS COLLECTION [TIME %s | TOTAL %s]" % (start_time_date.strftime('%Y_%m_%d-%H_%M_%S'),elapsed_time))

def getbus(resource_url, id_name, resource_id, api_key, data_directory, type = None):
    start_time = time.time()
    start_time_date = datetime.now()
    r = requests.get(resource_url, params={id_name : resource_id, 'apikey' : api_key, 'type' : type})
    if r.status_code == requests.codes.ok:
        #get json object
        items = r.json()
        conn = psycopg2.connect(dbname=DB_NAME, port=PORT, user=USER,password=PASSWORD, host=HOST)
        cur = conn.cursor()
        file_name = datetime.now().strftime('%Y_%m_%d_%H_%M_%S.csv')
        with open(data_directory + file_name, 'a') as file:
            writer = csv.writer(file, delimiter = ';')
            writer.writerow(BUS_HEADER)
            for i in items['result']:     
                try:       
                    row = [value for key, value in i.items()]
                    row_normalized = []
                    for x in row:
                        if isinstance(x,str):
                            row_normalized.append(x.strip())
                        else:
                            row_normalized.append(x)
                            # lon , lat
                    pointString = "POINT(%s %s)" % (row_normalized[3], row_normalized[0])
                    cur.execute("INSERT INTO buses(lines, time, brigade, geom,chunktime) VALUES (%s, %s, %s, ST_GeomFromText(%s, 4326),%s)", [row_normalized[1], row_normalized[4], row_normalized[2], pointString, start_time_date])
                    writer.writerow(row_normalized)
                except AttributeError as inst:
                    print(inst.args)      # arguments stored in .args
                    print(inst)    
        elapsed_time = time.time() - start_time
        cur.execute("INSERT INTO log(collection, time,chunktime) VALUES (%s, %s, %s)", ["Buses", elapsed_time, start_time_date])          
        conn.commit()
        cur.close()
        conn.close()
        print("BUSES COLLECTION [TIME %s | TOTAL %s]" % (start_time_date.strftime('%Y_%m_%d-%H_%M_%S'), elapsed_time))

if __name__ == '__main__':
  app.run()
