W folderze data_processor znajduje si� skrypt u�ywany na maszynie wirtualnej do pobierania danych z serwer�w ZTM, przekazywanie ich do bazy danych i zapisywanie do plik�w CSV.

Ka�dy zrzut danych (domy�lnie co 30 s) zapisywany jest do osobnego pliku odpowiednio dla tramwaj�w (przyk�ad data_processor/tram_data/2017_06_01_19_58_58.csv) oraz dla autobus�w (przyk�ad data_processor/bus_data/2017_06_01_19_32_29.csv). Nie wstawiamy wszystkich plik�w, ze wzgl�du na du�y rozmiar. Nie wszystkie by�y u�ywane do przeprowadzania analizy.

Wszystkie dane zgromadzone przez system umieszczone s� na serwerze PostgreSQL z dodatkiem PostGIS. Dane do przegl�dania (prawa czytania):
Server name: tramwaje.postgres.database.azure.com
user:reader_user@tramwaje
pass:wgm5bvho
Dost�p do bazy danych "history", tabele: trams_l oraz buses_l. Kolumna "chunktime" oznacza czas zrzutu danych na serwer.

Dane �atwo mo�na przegl�da� poprzez serwer API: http://tram-data-processor-api3.azurewebsites.net/documentation